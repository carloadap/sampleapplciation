﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SampleTask.Web.Models;

namespace SampleTask.Web.Controllers
{
    public class PlaceController : Controller
    {
        // GET: Place
        private static Places _places = new Places();
        public ActionResult Index()
        {
            return View(_places.PlaceList);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(PlaceModels placeModel)
        {
            _places.CreatePlace(placeModel);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string name)
        {
            if (name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlaceModels place = _places.GetPlace(name);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlaceModels place)
        {
            _places.UpdatePlace(place);
            return RedirectToAction("Index");
        }

        public ActionResult Details(string name)
        {
            if (name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlaceModels place = _places.GetPlace(name);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        public ActionResult Delete(string name)
        {
            if (name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlaceModels place = _places.GetPlace(name);
            if (place == null)
            {
                return HttpNotFound();
            }
            _places.RemovePlace(place);
            return RedirectToAction("Index");
        }

    }
}