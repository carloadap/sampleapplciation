﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SampleTask.Web.Startup))]
namespace SampleTask.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
