﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleTask.Web.Models
{
    public class Places
    {
        public Places()
        {
            PlaceList.Add(new PlaceModels
            {
                Name = "Ayala",
                Description = "Owned by Ayalas Brothers",
                CreatedDate = Convert.ToDateTime("6/22/1976"),
                CreatedBy = "Binay"
            });
            PlaceList.Add(new PlaceModels
            {
                Name = "Bonifacio Global City",
                Description = "Big Buildings.",
                CreatedDate = Convert.ToDateTime("6/11/2012"),
                CreatedBy = "BGC"

            });
        }

        public List<PlaceModels> PlaceList = new List<PlaceModels>();

        public void CreatePlace(PlaceModels placeModel)
        {
            PlaceList.Add(placeModel);
        }

        public void UpdatePlace(PlaceModels placeModel)
        {
            foreach (PlaceModels placeList in PlaceList)
            {
                if (placeList.Name == placeModel.Name)
                {
                    PlaceList.Remove(placeList);
                    PlaceList.Add(placeModel);
                    break;
                }
            }
        }

        public void RemovePlace(PlaceModels placeModel)
        {
            foreach (PlaceModels placeList in PlaceList)
            {
                if (placeList.Name == placeModel.Name)
                {
                    PlaceList.Remove(placeList);
                    break;
                }
            }
        }

        public PlaceModels GetPlace(string name)
        {
            PlaceModels placeModel = null;

            foreach (PlaceModels pm in PlaceList)
                if (pm.Name == name)
                    placeModel = pm;

            return placeModel;
        }
    }
}