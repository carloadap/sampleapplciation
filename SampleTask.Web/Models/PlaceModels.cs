﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SampleTask.Web.Models
{
    public class PlaceModels
    {
        [DisplayName("Place Name")]
        [Required (ErrorMessage = "Place Name is Required")]
        public string Name { get; set; }

        [Required]
        [StringLength(150)]
        public string Description { get; set; }

        [DisplayName("Created Date")]
        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }

        [DisplayName("Created By")]
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
    }
}